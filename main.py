import random
"""
Created on Tue Aug 20 11:47:24 2019

@author: Amman Habib
"""


def display(matrix):
    for row in matrix:
        print(row, '\n')


def revert(matrix, ship_track, count):
    """reverts the matrix where ships overlap"""
    while count > 0:
        row, col = ship_track.pop()
        matrix[row][col] = 0
        count -= 1

    return matrix, ship_track


def fill_col(matrix, row, col, ship_track):
    print('filling the columns')
    count = 0
    u = 1
    d = 1

    while count < 5:
        if matrix[row][col] == 0:
            if (row, col) in ship_track:
                revert(matrix, ship_track, count)
                return False
            matrix[row][col] = 1
            ship_track.append((row, col))
            count += 1
        elif 0 <= row + d < len(matrix):
            if (row+d, col) in ship_track:
                revert(matrix, ship_track, count)
                return False
            ship_track.append((row+d, col))
            matrix[row+d][col] = 1
            d += 1
            count += 1
        else:
            if 0 <= row - u < len(matrix):
                if (row - u, col) in ship_track:
                    return False
                ship_track.append((row - u, col))
                matrix[row-u][col] = 1
                u += 1
                count += 1

    display(matrix)
    return True





def fill_row(matrix, row, col, ship_track):
    print("filling row")
    count = 0
    r = 1
    l = 1

    while count < 5:
        if matrix[row][col] == 0:
            if (row, col) in ship_track:
                revert(matrix, ship_track, count)
                return False
            matrix[row][col] = 1
            ship_track.append((row, col))
            count += 1
        elif 0 <= col + r < len(matrix):
            if (row, col+r) in ship_track:
                revert(matrix, ship_track, count)
                return False
            ship_track.append((row, col+r))
            print(matrix[row][col+r])
            matrix[row][col+r] = 1
            r += 1
            count += 1
        else:
            if 0 <= col-l < len(matrix):
                if (row, col-l) in ship_track:
                    revert(matrix, ship_track, count)
                    return False

                ship_track.append((row, col-l))
                matrix[row][col - l] = 1
                l += 1
                count += 1
    return True


def summon_ships(matrix):
    """"function initializes ships randomly"""
    count = 0
    #gets quantity of ships to be initialized.
    quantity = random.randrange(1, 5)
    ship_track = []
    while count < quantity:
        row = random.randrange(0, len(matrix))
        col = random.randrange(0, len(matrix))
        pos = "H" if random.getrandbits(1) == 0 else "V"
        if pos == 'H':
            if fill_row(matrix, row, col, ship_track):
                count += 1
        else:
            if fill_col(matrix, row, col, ship_track):
                count += 1

    return ship_track


def initialize_ground(dimension):
    """function initializes matrix with given dimension"""
    matrix = []

    for i in range(0, dimension):
        row = []
        for j in range(0, dimension):
            row.append(0)
        matrix.append(list(row))
    return matrix


def start_game(matrix):
    """function initializes ships and runs the game"""
    ship_track = summon_ships(matrix)
    while True:
        print("kindly enter the guessed row")
        row = input()
        print("kindly enter the guessed col")
        col = input()

        if (row, col) in ship_track:
            print("Congratulation you have destroyed the ship")
            matrix[row][col] = 8
            return
        else:
            print("You missed the target", '\n', "Want to try again?")
            print("Press 1 to try again and 0 to exit")
            num = int(input())
            if num == 1:
                continue
            elif num == 0:
                break

    display(matrix)


def run():
    print(" WELCOME TO BATTLE SHIP GAME")
    print("PLease enter the field size you would like to play in.'\n'")
    print("Note: Field size must not be greater than 100.")
    num = int(input())
    print(num)
    matrix,ship_track = initialize_ground(num)
    start_game(matrix,ship_track)


if __name__ == '__main__':
    run()
